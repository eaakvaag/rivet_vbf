// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Analyses/MC_ParticleAnalysis.hh"
#include "Rivet/Analyses/MC_JetAnalysis.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/PromptFinalState.hh"
#include "Rivet/Projections/TauFinder.hh"
#include "Rivet/Projections/UnstableParticles.hh"
#include "Rivet/Projections/ZFinder.hh"


namespace Rivet {


  /// @brief MC validation analysis for taus
  class MC_TEST : public MC_ParticleAnalysis {
  public:

    /// Constructor
    MC_TEST()
      : MC_ParticleAnalysis("MC_TEST", 2, "tau")
    {    }


    /// Book projections and histograms
    void init() {
      TauFinder taus(TauFinder::DecayMode::ANY);
      declare(taus, "Taus");

      M2G = 1000;
      book(_h["hist_N_taus"],   "N_taus",   10, 0,  10);
      book(_h["hist_Higgs_pt"], "Higgs_pt", 20, 0, 400);
      book(_h["hist_Higgs_m"],  "Higgs_m",  20, 0, 200);


      for (size_t i = 0; i < _nparts; ++i) {

        const string ptname = _pname + "_pt_" + to_str(i);
        // const double ptmax = 1.0/(double(i)+2.0) * (sqrtS()>0.?sqrtS():14000.)/GeV/2.0;
        // const int nbins_pt = 20/(i+1);
        // book(_h_pt[i] ,ptname, logspace(nbins_pt, 1.0, ptmax));
        book(_h_pt[i] ,ptname, 20, 0, 300);
        const string etaname = _pname + "_eta_" + to_str(i);
        book(_h_eta[i] ,etaname, i > 1 ? 25 : 25, -5.0, 5.0);
        book(_h_eta_plus[i], "_" + etaname + "_plus", i > 1 ? 15 : 25, 0, 5);
        book(_h_eta_minus[i], "_" + etaname + "_minus", i > 1 ? 15 : 25, 0, 5);

        const string rapname = _pname + "_y_" + to_str(i);
        book(_h_rap[i] ,rapname, i > 1 ? 25 : 25, -5.0, 5.0);
        book(_h_rap_plus[i], "_" + rapname + "_plus", i > 1 ? 15 : 25, 0, 5);
        book(_h_rap_minus[i], "_" + rapname + "_minus", i > 1 ? 15 : 25, 0, 5);

        for (size_t j = i+1; j < min(size_t(3), _nparts); ++j) {
        const pair<size_t, size_t> ij = std::make_pair(i, j);

        string detaname = _pname + "s_deta_" + to_str(i) + to_str(j);
        Histo1DPtr tmpeta;
        book(tmpeta, detaname, 25, -5.0, 5.0);
        _h_deta.insert(make_pair(ij, tmpeta));

        string dphiname = _pname + "s_dphi_" + to_str(i) + to_str(j);
        Histo1DPtr tmpphi;
        book(tmpphi, dphiname, 25, 0.0, M_PI);
        _h_dphi.insert(make_pair(ij, tmpphi));

        string dRname = _pname + "s_dR_" + to_str(i) + to_str(j);
        Histo1DPtr tmpR;
        book(tmpR, dRname, 25, 0.0, 5.0);
        _h_dR.insert(make_pair(ij, tmpR));
        }
      }

      // jet collection

      FinalState fs_full;
      FastJets jets(fs_full, FastJets::ANTIKT, 0.4);
      declare(jets, "jets");

      book(_h["hist_Njets30"],   "N_jets_30",  10, 0,   10);
      book(_h["hist_jet_0_pt"],  "jet_0_pt",   20, 0,  400);
      book(_h["hist_jet_1_pt"],  "jet_1_pt",   20, 0,  400);
      // book(_h["hist_deltay_jj"], "deltay_jj",  25,  0, 5.0);
      book(_h["hist_deta_jj"],   "deta_jj",    25, 0,  5.0);
      book(_h["hist_dphi_jj"],   "dphi_jj",    25, 0, M_PI);
      book(_h["hist_dR_jj"],     "dR_jj",      25, 0,  5.0);
      book(_h["hist_m_jj"],      "dijet_mass", 20, 0,  400);


      //MC_ParticleAnalysis::init();
    }


    /// Per-event analysis
    void analyze(const Event& event) {
      //const Particles taus = apply<TauFinder>(event, "Taus").particlesByPt(0.5*GeV);
      const Particles& taus = apply<TauFinder>(event, "Taus").particles();

      const double weight = event.weight();
      Particles promptparticles;
      for (const Particle& p : taus)
        if (p.isPrompt()) promptparticles += p;
        //if (!p.fromDecay()) promptparticles += p;
        //std::cout<<weight<<std::endl;
      
      const Particles& prompt_taus = promptparticles;
      _h["hist_N_taus"]->fill(prompt_taus.size());
      if(prompt_taus.size()>=2){
        const double higgs_pt = (prompt_taus[0].mom() + prompt_taus[1].mom()).pt();
        _h["hist_Higgs_pt"]->fill(higgs_pt);
        const double higgs_m = (prompt_taus[0].mom() + prompt_taus[1].mom()).mass();
        _h["hist_Higgs_m"]->fill(higgs_m);
      }

      



      for (size_t i = 0; i < _nparts; ++i) {
        if (prompt_taus.size() < i+1) continue;
        _h_pt[i]->fill(prompt_taus[i].pt(), weight);

        // Eta
        const double eta_i = prompt_taus[i].eta();
        _h_eta[i]->fill(eta_i);
        (eta_i > 0.0 ? _h_eta_plus : _h_eta_minus)[i]->fill(fabs(eta_i));

        // Rapidity
        const double rap_i = prompt_taus[i].rapidity();
        _h_rap[i]->fill(rap_i);
        (rap_i > 0.0 ? _h_rap_plus : _h_rap_minus)[i]->fill(fabs(rap_i));
        // Inter-particle properties
        for (size_t j = i+1; j < min(size_t(3),_nparts); ++j) {
          if (prompt_taus.size() < j+1) continue;
          std::pair<size_t, size_t> ij = std::make_pair(i, j);
          double deta = prompt_taus[i].eta() - prompt_taus[j].eta();
          double dphi = deltaPhi(prompt_taus[i].momentum(), prompt_taus[j].momentum());
          double dR = deltaR(prompt_taus[i].momentum(), prompt_taus[j].momentum());
          _h_deta[ij]->fill(deta);
          _h_dphi[ij]->fill(dphi);
          _h_dR[ij]->fill(dR);
        }
      }

      const Jets& jets = apply<FastJets>(event, "jets").jetsByPt(Cuts::pT > 30);

      // Jet variables. Use jet collection with pT threshold at 30 GeV
      _h["hist_Njets30"]->fill(jets.size());

      if (jets.size()) _h["hist_jet_0_pt"]->fill(jets[0].pt());
      if (jets.size() >= 2) {
        const FourMomentum &j1 = jets[0].momentum(), &j2 = jets[1].momentum();
        _h["hist_jet_0_pt"]->fill(jets[0].pt());
        _h["hist_jet_1_pt"]->fill(jets[1].pt());
        // _h["hist_deltay_jj"]->fill(std::abs(j1.rapidity() - j2.rapidity()));
        _h["hist_deta_jj"]->fill(deltaEta(jets[0].momentum(), jets[1].momentum()));
        _h["hist_dphi_jj"]->fill(deltaPhi(jets[0].momentum(), jets[1].momentum()));
        _h["hist_dR_jj"]->fill(deltaR(jets[0].momentum(), jets[1].momentum()));
        _h["hist_m_jj"]->fill((j1 + j2).mass());
      }




      //MC_ParticleAnalysis::_analyze(event, taus);
    }

    void finalize() {
      const double scaling = crossSection()/sumOfWeights();

      scale(_h, scaling);

      for (size_t i = 0; i < _nparts; ++i) {
        scale(_h_pt[i], scaling);
        scale(_h_eta[i], scaling);
        scale(_h_rap[i], scaling);

        // Create eta/rapidity ratio plots
        // divide(_h_eta_plus[i], _h_eta_minus[i], tmpeta[i]);
        // divide(_h_rap_plus[i], _h_rap_minus[i], tmprap[i]);
      }

      // Scale the d{eta,phi,R} histograms
      typedef map<pair<size_t, size_t>, Histo1DPtr> HistMap;
      for (HistMap::value_type& it : _h_deta) scale(it.second, scaling);
      for (HistMap::value_type& it : _h_dphi) scale(it.second, scaling);
      for (HistMap::value_type& it : _h_dR) scale(it.second, scaling);


      //MC_ParticleAnalysis::finalize();


    }
    map<string, Histo1DPtr> _h;
    int M2G;

  };
  // The hook for the plugin system
  RIVET_DECLARE_PLUGIN(MC_TEST);

}


