import MadGraphControl.MadGraph_NNPDF30NLO_Base_Fragment
from MadGraphControl.MadGraphUtils import *

import os 
from tempfile import mkstemp
from shutil import move
from os import remove, close

def replace(file_path, pattern, subst):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    close(fh)
    remove(file_path)
    move(abs_path, file_path)


def replaceNextLine(file_path, pattern, strNextLine):
    fh, abs_path = mkstemp()
    with open(abs_path,'w') as new_file:
        with open(file_path) as old_file:
            found = False
            for line in old_file:
                if found:
                    new_file.write(strNextLine+"\n")
                    found = False
                else:
                    new_file.write(line)
                    if pattern in line:
                        found = True
    close(fh)
    remove(file_path)
    move(abs_path, file_path)


nevents = runArgs.maxEvents if runArgs.maxEvents>0 else evgenConfig.nEventsPerJob 
nevents*=1.1

process = """
    import model HC_NLO_X0_UFO-no_b_mass
    define p = g d d~ u u~ s s~ c c~ b b~
    define j = g d d~ u u~ s s~ c c~ b b~
    generate p p > x0 j j $$ w+ w- z / a [QCD]
    output -f
    """


beamEnergy=-999 
if hasattr(runArgs,'ecmEnergy'):
     beamEnergy = runArgs.ecmEnergy / 2. 
else:
     raise RuntimeError("No center of mass energy found.")


settings_run = {   'parton_shower': 'PYTHIA8',
                   'nevents':int(nevents), }


masses={'25': '1.250000e+02'}
parameters={
        'Lambda': 1000.0,
        'cosa'  : 1.0,
        'kSM'   : 1.0,
        'kHtt'  : 0.0,
        'kAtt'  : 0.0,
        'kHll'  : 0.0,
        'kAll'  : 0.0,
        'kHaa'  : 1.0,
        'kAaa'  : 0.0,
        'kHza'  : 0.0,
        'kAza'  : 0.0,
        'kHzz'  : 0.0,
        'kAzz'  : 0.0,
        'kHww'  : 0.0,
        'kAww'  : 0.0,
        'kHda'  : 0.0,
        'kHdz'  : 0.0,
        'kHdwR' : 0.0,
        'kHdwI' : 0.0
        }


params={}
params['MASS']=masses
params['FRBLOCK']=parameters




process_dir = new_process(process) 
modify_run_card(process_dir=process_dir,runArgs=runArgs,settings=settings_run) 
modify_param_card(process_dir=process_dir,params=params)

replace(os.path.join(process_dir,"bin/internal/amcatnlo_run_interface.py"), "tests.append('check_poles')", "pass #tests.append('check_poles')")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#IRPoleCheckThreshold", "-1")
replaceNextLine(os.path.join(process_dir,"Cards/FKS_params.dat"), "#PrecisionVirtualAtRunTime", "-1")



print_cards()

generate(process_dir=process_dir,runArgs=runArgs) 
arrange_output(process_dir=process_dir,runArgs=runArgs,lhe_version=3,saveProcDir=True)


evgenConfig.nEventsPerJob = 10000
include("Pythia8_i/Pythia8_A14_NNPDF23LO_EvtGen_Common.py")
include("Pythia8_i/Pythia8_MadGraph.py")


