theApp.EvtMax = -1

import re, os, glob
evntfiles = []
xssum=0.
effsum=0.
nfiles=0

for dir in glob.glob("/disk/atlas3/users/eaakvaag/vbf_analysis/generator_stuff/first_test/test_tutorial/run/htt/run_*"):
    evntfile=dir+"/EVNT.root"
    if os.path.exists(evntfile):
        with open(dir+"/log.generate") as logfile: 
            for line in logfile: 
                xsline = re.findall(r'MetaData: cross-section', line)
                if xsline: 
                    xsline = line.split(' ')[4]
                    xssum += float(xsline)
                    evntfiles.append(evntfile)
                    nfiles+=1
                effline = re.findall(r'MetaData: GenFiltEff', line)
                if effline: 
                    effline = line.split(' ')[4]
                    effsum += float(effline)

import AthenaPoolCnvSvc.ReadAthenaPool
#svcMgr.EventSelector.InputCollections = [ '/eos/atlas/atlascerngroupdisk/phys-gener/examples/rivet/EVNT.root' ]
svcMgr.EventSelector.InputCollections = evntfiles

from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

#from xAODEventInfoCnv.xAODEventInfoCnvConf import xAODMaker__EventInfoCnvAlg
#job += xAODMaker__EventInfoCnvAlg()

from Rivet_i.Rivet_iConf import Rivet_i

rivet = Rivet_i()
rivet.AnalysisPath = os.environ['PWD']
rivet.Analyses += [ 'MC_TEST' ]
rivet.RunName = ""
rivet.HistoFile = "Rivet.yoda"
rivet.CrossSection = 1.0
job += rivet

